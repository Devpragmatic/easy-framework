package com.easyframework.controller;

import org.springframework.web.bind.annotation.RestController;

/**
 * Main controller for all application. This is only way to connect with framework
 * 
 * @author Kamil Żur
 */
@RestController
public class MainController {
    
}
